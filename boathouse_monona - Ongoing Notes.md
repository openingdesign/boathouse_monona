
- [§ 480-24 Single Family Residence District](https://ecode360.com/30665760)
	- ~~District dimensional guidelines.~~
		- ~~(1) Minimum lot area: 10,000 square feet (unless lot was platted prior to adoption of this chapter).~~
		- ~~(2) Minimum lot frontage: 70 feet in width along a public street or 50 feet if abutting a public street with a curvature exceeding 33.3°.~~
		- ~~(3) Minimum lot width at building line: 60 feet.~~
		- ~~(4) Minimum setbacks.~~
			- ~~(a) Street yard: 30 feet (but if 40% or more of the frontage on one side of a street between two intersecting streets has been developed with a setback other than 30 feet, the street yard setback so established shall prevail).~~
			- ~~(b) Side yard: seven feet.~~
			- ~~(c\) Rear yard: 40 feet.~~
			- ~~(d) Shore yard: 50 feet from the meander line. (May be increased to average shore yard setback of two adjacent houses on both sides, up to seventy-five-foot maximum requirement.)~~
		- ~~(5) Maximum height: 35 feet.~~
		- ~~(6) Maximum lot coverage: 40%.~~
		- (7) Accessory buildings and structures.
			- (a) Maximum height: 14 feet.
			- (b) Distance. Must be no less than six feet from principal building.
			- (c\) Minimum setbacks.
				- [1] Street yard. Same as principal building.
				- [2] Side yard: three feet. Overhangs shall not project more than 1/3 into setback area.
				- [3] Rear yard: three feet. Overhangs shall not project more than 1/3 into setback area.
				- [4] Shore yard: None, but the only accessory buildings permitted in shore yards are boathouses, boat shelters, boat landings and piers.
			- (d) Lot coverage. Maximum total lot coverage of all accessory buildings on lot: 1,000 square feet.
		- ~~(8) Permitted encroachments into required yard setbacks.~~
			- ~~(a) Roof overhangs that are not supported to the ground may extend into any required yard by not more than 2.5 feet.~~
			- ~~(b) Uncovered steps not over three feet above the ground level which are necessary for access to a permitted building.~~
		- (9) Maximum impervious surface.[2]
			- (a) Lots over 10,000 square feet: 65%. A special exception permit may be granted by the Plan Commission up to **seventy-percent impervious surface** if the applicant demonstrates there is no substantial negative impact caused by the additional impervious surface to the adjoining water bodies or adjacent parcels as a result of stormwater runoff.
			- ~~(b) Lots less than 10,000 square feet: 70%. A special exception permit may be granted by the Plan Commission up to seventy-five-percent impervious surface if the applicant demonstrates there is no substantial negative impact caused by the additional impervious surface to the adjoining water bodies or adjacent parcels as a result of stormwater runoff.~~
			- ~~(c) Any request above and beyond the special exceptions allowed in Subsection D(9)(a) and (b) shall be reviewed as a variance request by the Zoning Board of Appeals.~~
		- (10) Grading requirements. The difference between the natural grade of the property and the finished grade of the property **shall not cumulatively exceed two feet at any point on the lot**, as shown on a submitted grading plan certified by a professional landscape architect, engineer or surveyor. A **special exception permit** may be granted by the Plan Commission for a grade change of up to eight feet, if the applicant demonstrates there is no substantial negative impact to adjoining water bodies or adjacent parcels. These regulations shall not prohibit compliance with floodplain development regulations. Any request above eight feet shall be reviewed as a variance request by the Zoning Board of Appeals.[3]





- [§ 466-28 Prohibited uses in shoreland-wetland](https://ecode360.com/30695937?highlight=boathouse&searchId=6574821155952760#30695937)
	- B. The use of a boathouse for **human habitation** and the construction or placement of a boathouse or fixed houseboat below the ordinary high-water mark of any navigable waters are prohibited.
- [Building Construction - Article IV - Accessory Structures Code](https://ecode360.com/30663075)
	- [§ 175-24 Boathouses, shelters, piers](https://ecode360.com/30663111?highlight=boathouses&searchId=6574703264337934#30663111)
		- Setback requirements.
			- (1) Boathouses or shelters built on property adjoining lagoons or the Yahara River within the boundaries of the City shall not extend beyond the mean water level of these waters.
			- (2) Piers and boat landings erected on any lagoon or river within the boundaries of the City **shall not extend more than six feet over the water**, unless a conditional use permit has been issued by the Zoning Board of Appeals.
			- (3) All piers and boat landings shall be set back a minimum of **seven feet from the side yard lot lines** and shall be erected so as to not impede or endanger boat traffic.
			- ~~(4) Notwithstanding the provisions of Subsection C(3), a joint pier between adjacent properties may be erected within the side yard setback lines of those properties under the following conditions:~~
				- ~~(a) The joint pier complies with all other requirements for an individual pier;~~
				- ~~(b) No other piers exist on the properties adjoining at the lot line to which the setback applies;~~
				- ~~(c) The joint pier extends from shore at only one location;~~
				- ~~(d) All owners of the adjacent properties between which the pier shall be erected shall sign the pier permit application.~~
- [Chapter 466 Floodplain and Shoreland-Wetland Zoning](https://ecode360.com/30695725)
- #### Questions
	- 1000 sf, include overhangs? does not
		- max on overhang? no max.  Can only encroach 1/3 into setback
					- difference between a floor that extends out from a boathouse, and a pier floor next to a boathouse, that goes over the water?
						- *(2) Piers and boat landings erected on any lagoon or river within the boundaries of the City **shall not extend more than six feet over the water**.  This does not apply, however, to this project.
	- New boat house?
		- conditional approval required for new or renovated? no.
	- Windows size/location restrictions? no restrictions
	- total height of structure? 14ft, from floor to deck--does not include railing



<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0ODEyNzIxNjksLTE4NDUzOTI2OTIsMT
kyMTc0MzAzMCwtMTM0MzUwMzc2NywtODc5NzQzMTg2LC03ODU3
NjQ4MTQsNjcxMDcwNzI0LDIwMDkwMjAyMzRdfQ==
-->